<div align=center><img src="https://gitee.com/WanYueKeJi/wanyue-kyhospital-web/raw/master/images/kyhospital.png" width="590" height="212"/></div>

<div align="center">
 
[![](https://img.shields.io/badge/%E9%83%A8%E7%BD%B2%E6%96%87%E6%A1%A3-%E7%82%B9%E5%87%BB%E6%9F%A5%E7%9C%8B-yellow)](https://www.kancloud.cn/wanyuekaiyuan11/kyhospital/3200052)
[![](https://img.shields.io/badge/QQ%E7%BE%A4-995910672-green)](https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi)

 

### 重要！重要！请点击上方按钮，查看“部署文档”！！！


------------------------------------------------------------------------
</div>  


### 项目说明及部署文档（如果对你有用，请点亮右上角的Star！）

##### <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/kyhospital/3200052">部署文档</a>  |  <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/kyhospital/3200052">发行步骤</a> | <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue-zhishi/2794476">常见问题</a> | <a target="_blank" href="https://www.kancloud.cn/wanyuekaiyuan11/wanyue-zhishi/2794476">升级日志</a>


 
### 系统演示

![输入图片说明](https://gitee.com/WanYueKeJi/wanyue-kyhospital-uniapp/raw/master/kyhospital-demo333.png)
    

 ### 演示地址

 - 后台地址: <a target="_blank" href="https://kyhospital.sdwanyue.com/admin">https://kyhospital.sdwanyue.com/admin</a> 账号: demo 密码: 123456
 - UNIAPP仓库地址: <a target="_blank" href="https://gitee.com/WanYueKeJi/wanyue-kyhospital-uniapp">点击此处</a>
 
  
 ### 项目介绍 
 
 万岳科技互联网医院系统，包括与HIS对接，智慧医院、远程诊疗、网上药房、电子处方流转等子系统，满足各级别医院开展互联网诊疗！

 万岳互联网医院系统深刻理解用户诉求，紧盯市场需求。帮助大家低成本高效率体验互联网医院平台，以利用互联网让人们生活更美好为使命，精益求精，创新研发，为客户创造更多价值！

 * 所有使用到的框架或者组件都是基于开源项目，代码保证100%开源。
 * 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的互联网医院系统。

 系统前端采用uni-app+socket.io+WebRtc核心技术, 接口采用PhalApi框架配合TP5.1框架ThinkCMF,系统功能如下:

 
 ### 功能展示
![输入图片说明](images/kyhospital1.png)
![输入图片说明](images/kyhospital2.png)
![输入图片说明](images/kyhospital3.png)

  ### 开源版使用须知

  - 需标注"代码来源于万岳科技开源项目"后即可免费自用运营
  - 前端运营时展示的内容不得使用万岳科技相关信息
  - 允许用于个人学习、教学案例
  - 开源版不得直接倒卖源码
  - 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负

  ### 说明

   * 如果你想使用功能更完善的互联网医院系统，请联系QQ客服: 2770722087 获取专业版
   * 如果您想基于互联网医院系统进行定制开发，我们提供有偿定制服务支持！
   * 其他合作模式不限，欢迎来撩！
   * 官网地址：[http://git.sdwanyue.com](http://git.sdwanyue.com)
                    
      
  ### 数据库（免费获取sql脚本）

    
<div style='height: 130px'>
        <img class="kefu_weixin" style="float:left;" src="https://gitee.com/WanYueKeJi/wanyue_education_uniapp/raw/newone/pages/%E5%BC%A0%E7%9A%93%E5%BC%80%E6%BA%90.png" width="602" height="123"/>
        <div style="float:left;">
            <p>QQ：2770722087</p>
          <p>QQ群：995910672</p>
          <p>QQ群：681418688</p>
        </div>
    </div>
    <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=JShAyXeoKqg2lWFEUSElxELImhjeMG4y&jump_from=webapi"><img border="0" src="https://images.gitee.com/uploads/images/2021/0317/100424_072ee536_8543696.png" alt="万岳在线教育讨论群" title="万岳在线教育讨论群"></a> 

  ###  开源交流群【加群回答请填写“gitee医院”】


![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_education_web/raw/master/%E4%B8%87%E5%B2%B3%E7%A7%91%E6%8A%80%E5%BC%80%E6%BA%90%E8%AE%A8%E8%AE%BA10%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)  ![输入图片说明](https://gitee.com/WanYueKeJi/wanyue_education_web/raw/master/%E4%B8%87%E5%B2%B3%E7%A7%91%E6%8A%80%E5%BC%80%E6%BA%90%E8%AE%A8%E8%AE%BA15%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)


    
